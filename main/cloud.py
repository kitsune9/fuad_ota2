from umqttsimple import MQTTClient
from machine import Pin
import machine
import ubinascii
 
 # Setup a GPIO Pin for output
bulbPin = Pin(2, Pin.OUT)
 
# Modify below section as required
CONFIG = {
     # Configuration details of the MQTT broker
     "MQTT_BROKER": "tailor.cloudmqtt.com",
     "USER": "tnjpscvk",
     "PASSWORD": "pTCFQDaQvqdr",
     "PORT": 17488,
     "TOPIC": b"room/light",
     # unique identifier of the chip
     "CLIENT_ID": b"esp32_" + ubinascii.hexlify(machine.unique_id())
}
 
# Method to act based on message received   
def onMessage(topic, msg):
    print("Topic: %s, Message: %s" % (topic, msg))
 
    if msg == b"1":
        bulbPin.value(1)
    elif msg == b"0":
        bulbPin.value(0)
 
def listen():
    #Create an instance of MQTTClient 
    client = MQTTClient(CONFIG['CLIENT_ID'], CONFIG['MQTT_BROKER'], user=CONFIG['USER'], password=CONFIG['PASSWORD'], port=CONFIG['PORT'])
    # Attach call back handler to be called on receiving messages
    client.set_callback(onMessage)
    client.connect()
    client.subscribe(CONFIG['TOPIC'])
    print("ESP32 is Connected to %s and subscribed to %s topic" % (CONFIG['MQTT_BROKER'], CONFIG['TOPIC']))
 
    try:
        while True:
            client.wait_msg()
    finally:
        client.disconnect()  
